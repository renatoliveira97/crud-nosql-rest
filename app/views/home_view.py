from flask import Flask, jsonify, request
from app.models.post_model import Post
from app.exceptions.posts_exceptions import InvalidPostError


def home_view(app: Flask):
    
    @app.get('/posts')
    def read_posts():
        posts_list = Post.get_all()
        return jsonify(posts_list), 200


    @app.get('/posts/<int:id>')
    def read_post_by_id(id):
        post = Post.get_post(id)
        if post != []:
            return str(post), 200
        else:
            return 'Post not found', 404

    @app.delete('/posts/<int:id>')
    def delete_post(id):
        try:
            Post.delete_post(id)
            return '', 204
        except IndexError as e:
            return f"message: {e.args}", 404


    @app.route('/posts/<int:id>', methods=["PATCH"])
    def update_post(id):
        data = request.json
        Post.update_post(id, **data)
        return '', 200
        

    @app.post('/posts')
    def create_post():

        data = request.json

        try:
            post = Post(**data)
            new_post = post.save()
            return new_post, 201
        except (InvalidPostError, TypeError):
            return {'message': 'Dados Inválidos para a criação de um post.'}, 400
