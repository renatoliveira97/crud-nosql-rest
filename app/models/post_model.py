from pymongo import MongoClient
from app.exceptions.posts_exceptions import InvalidPostError
from bson.objectid import ObjectId


client = MongoClient('mongodb://localhost:27017/')

db = client['kenzie']


class Post():

    def __init__(self, id: int, title: str, author: str, tags: list, content: str):
        self.id = id
        self.title = title
        self.author = author
        self.tags = tags
        self.content = content


    @staticmethod
    def get_all():
        posts_list = list(db.posts.find())
        for post in posts_list:
            del post['_id']
        return posts_list


    @staticmethod
    def get_post(id):
        posts_list = list(db.posts.find())
        post = [item for item in posts_list if item["id"] == id]
        return post

    @staticmethod
    def delete_post(id):
        posts_list = list(db.posts.find())
        post = [item for item in posts_list if item["id"] == id]
        return db.posts.delete_one(post[0])

    
    @staticmethod
    def update_post(id, data):
        update = {"$set": data}
        posts_list = list(db.posts.find())
        post = [item for item in posts_list if item["id"] == id]
        return db.posts.update_one(post, update)


    def save(self):
        _id = db.posts.insert_one(self.__dict__).inserted_id

        if not _id:
            raise InvalidPostError
        
        new_post = db.posts.find_one({'_id': _id})

        del new_post['_id']

        return new_post
